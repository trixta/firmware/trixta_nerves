defmodule TrixtaNerves.Pins.PinUtils do
    require Logger
    alias Circuits.GPIO

    def digital_write(pin_number, enabled) when is_integer(pin_number) and enabled in [0,1] do
        Logger.debug("Setting pin #{pin_number} to #{enabled}")
        {:ok, gpio} = Circuits.GPIO.open(pin_number, :output)
        Circuits.GPIO.write(gpio, enabled)
        Circuits.GPIO.close(gpio)
    end

    def digital_write([_ | _] = pins, enabled) when enabled in [0,1] do
        pins |> Enum.each(&digital_write(&1, enabled))
    end

    def digital_read(pin_number) when is_integer(pin_number) do
        {:ok, gpio} = Circuits.GPIO.open(pin_number, :input)
        Circuits.GPIO.read(gpio)
    end
  end
  