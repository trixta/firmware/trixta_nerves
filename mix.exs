defmodule TrixtaNerves.MixProject do
  use Mix.Project

  def project do
    [
      app: :trixta_nerves,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      description: description(),
      package: package(),
      deps: deps(),
      aliases: aliases(),
      name: "TrixtaNerves",
      docs: [main: "TrixtaNerves", extras: ["README.md"]],
      dialyzer: [plt_add_deps: :transitive],
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ]
      # if you want to use espec,
      # test_coverage: [tool: ExCoveralls, test_task: "espec"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {TrixtaNerves.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:credo, "~> 1.1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 0.5", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.12", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.21", only: [:dev, :test], runtime: false},
      {:inch_ex, "~> 2.0", only: [:dev, :test], runtime: false}
    ]
  end

  def aliases do
    [
      review: ["coveralls", "dialyzer", "inch", "hex.audit", "hex.outdated", "credo --strict"]
    ]
  end

  defp description() do
    "Generates a unique, URL-friendly name such as \"vast-north-8503\"."
  end

  defp package() do
    [
      licenses: ["AGPL-3.0-or-later"],
      links: %{"GitLab" => "https://gitlab.com/trixta/firmware/trixta_nerves"}
    ]
  end
end
