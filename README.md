# TrixtaNerves

> Nerves based device interaction for Trixta.

[![pipeline status](https://gitlab.com/trixta/firmware/trixta_nerves/badges/master/pipeline.svg)](https://gitlab.com/trixta/firmware/trixta_nerves/commits/master)
[![coverage report](https://gitlab.com/trixta/firmware/trixta_nerves/badges/master/coverage.svg)](https://gitlab.com/trixta/firmware/trixta_nerves/commits/master)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v1.4%20adopted-ff69b4.svg)](code-of-conduct.md)
[![Maintenance](https://img.shields.io/maintenance/yes/2019.svg)]()
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `trixta_nerves` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:trixta_nerves, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/trixta_nerves](https://hexdocs.pm/trixta_nerves).

## Contributing

PRs accepted. See [CONTRIBUTING](./CONTRIBUTING.md).

## License

© Trixta Inc. See [LICENSE](./LICENSE).
